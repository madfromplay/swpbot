package core

import (
	"context"
	"fmt"
	"log"

	tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api/v5"
	"gitlab.com/madfromplay/swpbot/internal/ctxtools"
	"gitlab.com/madfromplay/swpbot/internal/transactions"
)

func mutateMessage(ctx context.Context, update tgbotapi.Update, text string) {
	bot := ctx.Value(ctxtools.CtxBot{}).(*tgbotapi.BotAPI)
	t := ctx.Value(ctxtools.CtxTransationInstance{}).(*transactions.Transaction)
	if t != nil {
		text = fmt.Sprintf("%v \n\n %v", t.ToString(), text)
	}
	changeTextMsg := tgbotapi.NewEditMessageText(update.CallbackQuery.Message.Chat.ID, update.CallbackQuery.Message.MessageID, text)
	bot.Send(changeTextMsg)
}

func mutateMarkup(ctx context.Context, update tgbotapi.Update, markup tgbotapi.InlineKeyboardMarkup) {
	bot := ctx.Value(ctxtools.CtxBot{}).(*tgbotapi.BotAPI)
	newMarkup := tgbotapi.NewEditMessageReplyMarkup(update.CallbackQuery.Message.Chat.ID, update.CallbackQuery.Message.MessageID, markup)
	bot.Send(newMarkup)
}

type applyData func(transaction *transactions.Transaction, callbackData interface{})

type MenuInstance interface {
	Draw(ctx context.Context, update tgbotapi.Update)
	GetTriggers() []string
	GetMenuID() string
	Send(ctx context.Context, update tgbotapi.Update) (int, error)
	Apply(transaction *transactions.Transaction, callbackData interface{})
}

type Root struct {
	Childs []MenuInstance
}

func (m *Root) GetMenuInstanceByTrigger(trigger string) MenuInstance {
	for _, x := range m.Childs {
		for _, t := range x.GetTriggers() {
			if t == trigger {
				return x
			}
		}
	}
	return nil
}

func (m *Root) GetChildrenByMenuID(menuID string) MenuInstance {
	for _, x := range m.Childs {
		if x.GetMenuID() == menuID {
			return x
		}
	}
	return nil
}

type KeyboardMenu struct {
	MenuID            string
	Triggers          []string
	Keyboard          tgbotapi.InlineKeyboardMarkup
	Message           string
	updateTransaction applyData
}

func (m *KeyboardMenu) Apply(transaction *transactions.Transaction, callbackData interface{}) {
	m.updateTransaction(transaction, callbackData)
}

func (m *KeyboardMenu) Draw(ctx context.Context, update tgbotapi.Update) {
	if m != nil {
		mutateMessage(ctx, update, m.Message)
		mutateMarkup(ctx, update, m.Keyboard)

	}
}

func (m *KeyboardMenu) GetTriggers() []string {
	return m.Triggers
}

func (m *KeyboardMenu) GetMenuID() string {
	return m.MenuID
}

func (m *KeyboardMenu) Send(ctx context.Context, update tgbotapi.Update) (int, error) {
	bot := ctx.Value(ctxtools.CtxBot{}).(*tgbotapi.BotAPI)
	msg := tgbotapi.NewMessage(update.Message.Chat.ID, m.Message)
	msg.ReplyMarkup = CurrenyMenu.Keyboard
	message, err := bot.Send(msg)
	if err != nil {
		log.Println(err)
		return 0, err
	}
	return message.MessageID, nil
}

type TextMenu struct {
	MenuID            string
	Triggers          []string
	Message           string
	updateTransaction applyData
}

func (m *TextMenu) Apply(transaction *transactions.Transaction, callbackData interface{}) {
	m.updateTransaction(transaction, callbackData)
}

func (m *TextMenu) Draw(ctx context.Context, update tgbotapi.Update) {
	if m != nil {
		mutateMessage(ctx, update, m.Message)
	}
}

func (m *TextMenu) GetTriggers() []string {
	return m.Triggers
}

func (m *TextMenu) GetMenuID() string {
	return m.MenuID
}

func (m *TextMenu) Send(ctx context.Context, update tgbotapi.Update) (int, error) {
	bot := ctx.Value(ctxtools.CtxBot{}).(*tgbotapi.BotAPI)
	msg := tgbotapi.NewMessage(update.Message.Chat.ID, m.Message)
	msg.ReplyMarkup = CurrenyMenu.Keyboard
	message, err := bot.Send(msg)
	if err != nil {
		log.Println(err)
		return 0, err
	}
	return message.MessageID, nil
}

var MenuRoot = &Root{
	Childs: []MenuInstance{
		CurrenyMenu,
		OperationMenu,
		IncomeTypeMenu,
		ConfirmationMenu,
		TargetIncomeMenu,
		TargetExpenseMenu,
		ThankYouMenu,
	},
}

var OperationMenu = &KeyboardMenu{
	Triggers: []string{"gel", "dollar", "uah", "rub"},
	Keyboard: tgbotapi.NewInlineKeyboardMarkup(
		tgbotapi.NewInlineKeyboardRow(
			tgbotapi.NewInlineKeyboardButtonData("Поступление", "income:incomeType"),
			tgbotapi.NewInlineKeyboardButtonData("Расход", "expense:targetExpense"),
			tgbotapi.NewInlineKeyboardButtonData("Донат", "donation:targetExpense"),
		),
	),
	Message: "Выберите тип операции",
	MenuID:  "operation",
	updateTransaction: func(transaction *transactions.Transaction, callbackData interface{}) {
		transaction.TransactionType = callbackData.(string)
	},
}

var IncomeTypeMenu = &KeyboardMenu{
	Triggers: []string{"income"},
	Keyboard: tgbotapi.NewInlineKeyboardMarkup(
		tgbotapi.NewInlineKeyboardRow(
			tgbotapi.NewInlineKeyboardButtonData("Tinkoff", "tinkoff:targetIncome"),
			tgbotapi.NewInlineKeyboardButtonData("Monobank", "monobank:targetIncome"),
			tgbotapi.NewInlineKeyboardButtonData("Наличные", "cash:targetIncome"),
		),
		tgbotapi.NewInlineKeyboardRow(
			tgbotapi.NewInlineKeyboardButtonData("Boosty", "boosty:targetIncome"),
			tgbotapi.NewInlineKeyboardButtonData("BOG", "bog:targetIncome"),
			tgbotapi.NewInlineKeyboardButtonData("Крипта", "crypto:targetIncome"),
		),
	),
	Message: "Выберите тип поступления",
	MenuID:  "incomeType",
	updateTransaction: func(transaction *transactions.Transaction, callbackData interface{}) {
		transaction.IncomeType = callbackData.(string)
	},
}

var ConfirmationMenu = &KeyboardMenu{
	Triggers: []string{"common", "cats", "food", "vacvine", "neut", "vet", "common", "profilactic", "food", "vacvine", "neut", "vet", "meds", "construction"},
	Keyboard: tgbotapi.NewInlineKeyboardMarkup(
		tgbotapi.NewInlineKeyboardRow(
			tgbotapi.NewInlineKeyboardButtonData("Подтвердить", "accept:thankYou"),
			tgbotapi.NewInlineKeyboardButtonData("Отклонить", "decline:decline"),
		),
	),
	Message: "Подтвердите операцию",
	MenuID:  "confirmation",
	updateTransaction: func(transaction *transactions.Transaction, callbackData interface{}) {
		if callbackData.(string) == "accept" {
			transaction.TransactionStatus = "closed"
			return
		} else {
			transaction.TransactionStatus = "declined"
			return
		}

	},
}

var CurrenyMenu = &KeyboardMenu{
	Triggers: []string{},
	Keyboard: tgbotapi.NewInlineKeyboardMarkup(
		tgbotapi.NewInlineKeyboardRow(
			tgbotapi.NewInlineKeyboardButtonData("₾ gel", "gel:operation"),
			tgbotapi.NewInlineKeyboardButtonData("$ usd", "usd:operation"),
			tgbotapi.NewInlineKeyboardButtonData("₴ uah", "uah:operation"),
			tgbotapi.NewInlineKeyboardButtonData("₽ rub", "rub:operation"),
		),
	),
	Message: "Выберите валюту",
	MenuID:  "currency",
	updateTransaction: func(transaction *transactions.Transaction, callbackData interface{}) {
		transaction.Currency = callbackData.(string)
	},
}

var TargetIncomeMenu = &KeyboardMenu{
	Triggers: []string{"tinkoff", "monobank", "cash", "boosty", "bog", "crypto"},
	Keyboard: tgbotapi.NewInlineKeyboardMarkup(
		tgbotapi.NewInlineKeyboardRow(
			tgbotapi.NewInlineKeyboardButtonData("Общее", "common:confirmation"),
			tgbotapi.NewInlineKeyboardButtonData("Кошки", "cats:confirmation"),
			tgbotapi.NewInlineKeyboardButtonData("Корм", "food:confirmation"),
		),
		tgbotapi.NewInlineKeyboardRow(
			tgbotapi.NewInlineKeyboardButtonData("Вакцинация", "vacvine:confirmation"),
			tgbotapi.NewInlineKeyboardButtonData("Стерилизация", "neut:confirmation"),
			tgbotapi.NewInlineKeyboardButtonData("Лечение", "vet:confirmation"),
		),
	),
	Message: "Выберите категорию поступления",
	MenuID:  "targetIncome",
	updateTransaction: func(transaction *transactions.Transaction, callbackData interface{}) {
		transaction.IncomeTarget = callbackData.(string)
	},
}

var TargetExpenseMenu = &KeyboardMenu{
	Triggers: []string{"expense"},
	Keyboard: tgbotapi.NewInlineKeyboardMarkup(
		tgbotapi.NewInlineKeyboardRow(
			tgbotapi.NewInlineKeyboardButtonData("Хозрасходы", "common:confirmation"),
			tgbotapi.NewInlineKeyboardButtonData("Обработка", "profilactic:confirmation"),
			tgbotapi.NewInlineKeyboardButtonData("Корм", "food:confirmation"), //
		),
		tgbotapi.NewInlineKeyboardRow(
			tgbotapi.NewInlineKeyboardButtonData("Вакцинация", "vacvine:confirmation"),
			tgbotapi.NewInlineKeyboardButtonData("Стерилизация", "neut:confirmation"),
			tgbotapi.NewInlineKeyboardButtonData("Лечение", "vet:confirmation"),
		),
		tgbotapi.NewInlineKeyboardRow(
			tgbotapi.NewInlineKeyboardButtonData("Медицинские препараты и расходники", "meds:confirmation"),
			tgbotapi.NewInlineKeyboardButtonData("Строительство", "construction:confirmation"),
		),
	),
	Message: "Выберите категорию расходов",
	MenuID:  "targetExpense",
	updateTransaction: func(transaction *transactions.Transaction, callbackData interface{}) {
		transaction.ExpenseTarget = callbackData.(string)
	},
}

var ThankYouMenu = &TextMenu{
	Triggers: []string{"accept"},
	Message:  "Спасибо!",
	MenuID:   "thankYou",
	updateTransaction: func(transaction *transactions.Transaction, callbackData interface{}) {
	},
}

var DeclineMenu = &TextMenu{
	Triggers: []string{"accept"},
	Message:  "Транзакция отменена",
	MenuID:   "decline",
	updateTransaction: func(transaction *transactions.Transaction, callbackData interface{}) {
	},
}
