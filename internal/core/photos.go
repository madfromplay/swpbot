package core

import (
	"context"
	"log"

	tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api/v5"
)

func mod(x int) int {
	if x < 0 {
		return -x
	}
	return x
}

func getMinMaxMidv2(array []tgbotapi.PhotoSize) []tgbotapi.PhotoSize {
	var min, mid, max tgbotapi.PhotoSize
	var sum, avg, distance int

	for i, item := range array {
		sum += item.FileSize
		if i == 0 {
			min = item
			max = item
			continue
		}
		if item.FileSize < min.FileSize {
			min = item
		}
		if item.FileSize > max.FileSize {
			max = item
		}
	}

	avg = sum / len(array)

	for i, item := range array {
		current_distance := mod(item.FileSize - avg)
		if i == 0 {
			mid = item
			distance = current_distance
			continue
		}
		if current_distance < distance {
			mid = item
			distance = current_distance
		}
	}
	return []tgbotapi.PhotoSize{min, mid, max}
}

func processMessageWithPhoto(ctx context.Context, update tgbotapi.Update) {
	// get uniq photos from update
	var photos = make(map[string][]tgbotapi.PhotoSize)
	for _, photoSize := range update.Message.Photo {
		photos[photoSize.FileID] = append(photos[photoSize.FileID], photoSize)
	}
	// get targets to process from all photos
	for key, _ := range photos {
		targets := getMinMaxMidv2(photos[key])
	}

	for _, photoSize := range update.Message.Photo {
		// GET JSON ABOUT OUR IMG (ORDER TO GET FILE_PATH)
		log.Println(update.Message.MessageID, photoSize)
		// imgFileInfoUrl := fmt.Sprintf(getFileUrl, bot.Token, photoSize.FileID)
		// rr, err := http.Get(imgFileInfoUrl)
		// if err != nil {
		// 	log.Println("unable retrieve img by FileID", err)
		// 	return
		// }
		// defer rr.Body.Close()
		// fileInfoJson, err := io.ReadAll(rr.Body)
		// if err != nil {
		// 	log.Println("unable read img by FileID", err)
		// 	return
		// }

		// imgInfo := &imgFileInfo{}
		// err = json.Unmarshal(fileInfoJson, imgInfo)
		// log.Println(imgInfo)
		// if err != nil {
		// 	log.Println("unable unmarshal file description from api.telegram by url: "+imgFileInfoUrl, err)
		// }
		// // // GET FILE_PATH

		// fileUrl := fmt.Sprintf(downloadFileUrl, bot.Token, imgInfo.Result.FilePath)
		// downloadResponse, err := http.Get(fileUrl)
		// if err != nil {
		// 	log.Printf("unable download file by file_path: %v Error %v", downloadFileUrl, err)
		// 	return
		// }
		// defer downloadResponse.Body.Close()
		// // Save the image file
		// imageData, err := io.ReadAll(downloadResponse.Body)
		// if err != nil {
		// 	log.Println("unable to read image data", err)
		// 	return
		// }

		// err = os.WriteFile(imgInfo.Result.FilePath, imageData, 0644)
		// if err != nil {
		// 	log.Println("unable to save image file", err)
		// 	return
		// }
	}
}
