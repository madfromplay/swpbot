package core

import (
	"context"
	"log"
	"strconv"
	"strings"

	tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api/v5"
	"gitlab.com/madfromplay/swpbot/internal/ctxtools"
	"gitlab.com/madfromplay/swpbot/internal/transactions"
)

const getFileUrl = "https://api.telegram.org/bot%s/getFile?file_id=%s"
const downloadFileUrl = "https://api.telegram.org/file/bot%s/%s"

// const sendMsgToUserUrl = "https://api.telegram.org/bot%s/sendMessage"

type imgFileInfo struct {
	Ok     bool `json:"ok"`
	Result struct {
		FileId       string `json:"file_id"`
		FileUniqueId string `json:"file_unique_id"`
		FileSize     int    `json:"file_size"`
		FilePath     string `json:"file_path"`
	} `json:"result"`
}

type imgData struct {
	Data []byte
	Path string
	Size string
}

func newTransaction(ctx context.Context, amount float64, chatId int64, messageId int, userId int64) error {
	transactionService := ctx.Value(ctxtools.CtxTransactionService{}).(*transactions.TransactionService)

	transaction := &transactions.Transaction{
		Amount:    amount,
		ChatID:    chatId,
		MessageID: messageId,
		UserID:    userId,
		MenuID:    "currency",
	}
	err := transactionService.CreateTransaction(transaction)
	if err != nil {
		return err
	}
	return nil
}

func ProcessMessage(ctx context.Context) {
	bot := ctx.Value(ctxtools.CtxBot{}).(*tgbotapi.BotAPI)
	update := ctx.Value(ctxtools.CtxUpdate{}).(tgbotapi.Update)
	transactionService := ctx.Value(ctxtools.CtxTransactionService{}).(*transactions.TransactionService)
	if update.Message != nil {
		switch update.Message.Command() {
		case "new":
			log.Printf("New transaction command. MessageID: %v UserID %v ChatID %v",
				update.Message.MessageID,
				update.Message.From.ID,
				update.Message.Chat.ID)
			if update.Message.IsCommand() {
				// check args correctness for new transaction
				args, err := strconv.ParseFloat(update.Message.CommandArguments(), 64)
				if args == 0 || err != nil {
					msg := tgbotapi.NewMessage(update.Message.Chat.ID, "Пожалуйста используте команду /new <сумма>\nСумма должна быть больше 0\nПример\n/new 100")
					bot.Send(msg)
					return
				}
				offset, err := CurrenyMenu.Send(ctx, update)
				if err != nil {
					log.Println(err)
					return
				}
				err = newTransaction(ctx, args, update.Message.Chat.ID, offset, update.Message.From.ID)
				if err != nil {
					log.Println(err)
					return
				}
				return
			}
		case "comment":
			t, err := transactionService.GetTransaction(update.Message.ReplyToMessage.MessageID, update.Message.Chat.ID, update.Message.From.ID)
			if err != nil {
				log.Println(err)
				return
			}
			log.Printf("New comment command. MessageID: %v UserID %v ChatID %v Text %v",
				update.Message.MessageID,
				update.Message.From.ID,
				update.Message.Chat.ID,
				update.Message.CommandArguments())
			transactionService.NewComment(t.ID, update.Message.CommandArguments())
		case "photo":
		}
		if update.Message.Photo != nil {
			processMessageWithPhoto(ctx, update)
		}
		return
	} else if update.CallbackQuery != nil {
		t, err := transactionService.GetTransaction(update.CallbackQuery.Message.MessageID, update.CallbackQuery.Message.Chat.ID, update.CallbackQuery.From.ID)
		ctx = context.WithValue(ctx, ctxtools.CtxTransationInstance{}, t)
		if err != nil {
			log.Println(err)
			return
		}
		callbackData := strings.Split(update.CallbackQuery.Data, ":")[0]

		MenuRoot.GetChildrenByMenuID(t.MenuID).Apply(t, callbackData)
		nextMenuID := strings.Split(update.CallbackQuery.Data, ":")[1]
		menu := MenuRoot.GetChildrenByMenuID(nextMenuID)
		t.MenuID = nextMenuID
		transactionService.UpdateTransaction(t)
		if menu == nil {
			log.Println("Menu not found")
			return
		}
		menu.Draw(ctx, update)
	}
}
