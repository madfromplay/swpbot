package transactions

import (
	"fmt"
	"log"

	"gorm.io/gorm"
)

type Comment struct {
	gorm.Model
	TransactionID uint
	Text          string
}

type Transaction struct {
	gorm.Model
	MessageID         int
	ChatID            int64
	UserID            int64
	MenuID            string
	Currency          string
	Amount            float64
	TransactionType   string
	TransactionStatus string
	Comments          []Comment
	IncomeType        string
	IncomeTarget      string
	ExpenseTarget     string
}

func (t *Transaction) ToString() string {
	var str string
	if t.Amount != 0 {
		str += fmt.Sprintf("Amount: %.2f\n", t.Amount)
	}
	if t.Currency != "" {
		str += "Валюта: " + t.Currency + "\n"
	}
	if t.TransactionType != "" {
		str += "Тип: " + t.TransactionType + "\n"
	}
	if t.IncomeType != "" {
		str += "Тип поступления: " + t.IncomeType + "\n"
	}
	if t.IncomeTarget != "" {
		str += "Цель поступления: " + t.IncomeTarget + "\n"
	}
	if t.ExpenseTarget != "" {
		str += "Цель расходов: " + t.ExpenseTarget + "\n"
	}
	return str
}

type TransactionService struct {
	db *gorm.DB
}

func NewTransactionService(db *gorm.DB) *TransactionService {
	return &TransactionService{db: db}
}

func (t *TransactionService) NewComment(transactionID uint, comment string) error {
	var newComment Comment
	newComment.TransactionID = transactionID
	newComment.Text = comment
	log.Println(newComment)
	return t.db.Create(&newComment).Error
}

func (t *TransactionService) Migrate() error {
	return t.db.AutoMigrate(&Transaction{}, &Comment{})
}

func (t *TransactionService) CreateTransaction(transaction *Transaction) error {
	return t.db.Create(transaction).Error
}

func (t *TransactionService) GetTransaction(MessageID int, ChatID int64, UserID int64) (*Transaction, error) {
	var transaction Transaction
	transaction.MessageID = MessageID
	transaction.ChatID = ChatID
	transaction.UserID = UserID
	err := t.db.Where(&transaction).First(&transaction).Error
	return &transaction, err
}

func (t *TransactionService) UpdateTransaction(transaction *Transaction) error {
	return t.db.Save(transaction).Error
}
