package randomstring

import (
	"math/rand"
)

func GenerateRandomString(l int) string {
	const letters = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890"
	result := make([]byte, l)
	for i := 0; i < l; i++ {
		k := rand.Intn(len(letters))
		result[i] = letters[k]
	}
	return string(result)
}
