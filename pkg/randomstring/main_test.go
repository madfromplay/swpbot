package randomstring

import (
	"strings"
	"testing"
)

func TestGenerateRandomString(t *testing.T) {
	const letters = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890"

	testCases := []struct {
		length int
	}{
		{10},
		{0},
		// Add other test cases if needed
	}

	for _, tc := range testCases {
		result := GenerateRandomString(tc.length)

		// Check length of generated string
		if got := len(result); got != tc.length {
			t.Errorf("GenerateRandomString() = %v, want %v", got, tc.length)
		}

		// Check if all characters are from the correct set
		for _, char := range result {
			if !strings.ContainsRune(letters, char) {
				t.Errorf("GenerateRandomString() contains invalid character = %v", char)
			}
		}
	}
}
