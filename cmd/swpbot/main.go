package main

import (
	"context"
	"log"
	"os"

	tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api/v5"
	core "gitlab.com/madfromplay/swpbot/internal/core"
	ctxtools "gitlab.com/madfromplay/swpbot/internal/ctxtools"
	"gitlab.com/madfromplay/swpbot/internal/transactions"
	"gorm.io/driver/mysql"
	"gorm.io/gorm"
)

func main() {
	bot, err := tgbotapi.NewBotAPI(os.Getenv("TELEGRAM_APITOKEN"))
	if err != nil {
		log.Panic(err)
	}

	bot.Debug = false

	log.Printf("Authorized on account %s", bot.Self.UserName)

	u := tgbotapi.NewUpdate(0)
	u.Timeout = 60

	updates := bot.GetUpdatesChan(u)
	c := context.Background()

	dsn := "groyne-api:groyne-password@tcp(127.0.0.1:3306)/groyne?charset=utf8mb4&parseTime=True&loc=Local"
	db, err := gorm.Open(mysql.Open(dsn), &gorm.Config{})
	if err != nil {
		log.Fatal(err)
	}

	transactions := transactions.NewTransactionService(db)
	transactions.Migrate()

	for update := range updates {
		go func(update tgbotapi.Update) {
			log.Println("got update", update.Message.MessageID)
			ctx := context.WithValue(c, ctxtools.CtxBot{}, bot)
			ctx = context.WithValue(ctx, ctxtools.CtxUpdate{}, update)
			ctx = context.WithValue(ctx, ctxtools.CtxTransactionService{}, transactions)
			core.ProcessMessage(ctx)
		}(update)
	}
}
